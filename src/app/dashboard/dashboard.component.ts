import { Component, OnInit, Input } from '@angular/core';
import { IStorageService } from '../app-service/IStorage.service';
import { Note } from '../Models/Note';
import { NoteLook } from '../Models/NoteLook';
import { NoteType } from '../Models/NoteType';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardContainerComponent implements OnInit {
  notes: Observable<Note[]>;
  noteLook: NoteLook;
  noteType: NoteType;

  ngOnInit(): void {
    this.notes = this.storageService.getNotes(NoteType.notes);
  }

  constructor(private storageService: IStorageService) {
    this.noteLook = this.noteLook = this.storageService.noteLook;
    this.noteType = NoteType.notes;
  }
}
