import { Component, Input, OnInit } from '@angular/core';
import { Note } from '../Models/Note';
import { IStorageService } from '../app-service/IStorage.service';
import { NoteType } from '../Models/NoteType';
import { NoteLook } from '../Models/NoteLook';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reminders',
  templateUrl: './reminders.component.html',
  styleUrls: ['./reminders.component.scss'],
})
export class RemindersComponent implements OnInit {
  @Input() reminderNotes: Observable<Note[]>;
  noteLook: NoteLook;
  editorNote: Note;
  noteType: NoteType;

  constructor(private storageService: IStorageService) {
    this.reminderNotes = this.storageService.getNotes(NoteType.reminder);
    this.noteLook = this.storageService.noteLook;
    this.noteType = NoteType.reminder;
  }

  ngOnInit(): void {
    this.editorNote = this.reminderNotes[0] || null;
  }
}
