import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NoteLook } from '../Models/NoteLook';
import { Note } from '../Models/Note';
import { NoteType } from 'src/app/Models/NoteType';
import { IStorageService } from '../app-service/IStorage.service';
import { filter, map } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-tagged',
  templateUrl: './tagged.component.html',
  styleUrls: ['./tagged.component.scss'],
})
export class TaggedComponent implements OnInit {
  notes: Observable<Note[]>;
  noteLook: NoteLook;
  label: string;
  path: string;
  noteType: string;

  ngOnInit(): void {}

  constructor(private storageService: IStorageService, private router: Router) {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => {
          this.path = window.location.pathname;
          this.noteType = 'labelled';
          this.label = this.path.substr(8);
          this.notes = this.storageService.getNotesByLabel(this.label);
        }),
      )
      .subscribe();
  }
}
