import { Observable, BehaviorSubject } from 'rxjs';
import { Note } from '../Models/Note';
import { NoteType } from '../Models/NoteType';
import { NoteLook } from '../Models/NoteLook';

export abstract class IStorageService {
  abstract noteLook: NoteLook;
  abstract getNotesByLabel(label: string): Observable<Note[]>;
  abstract createLabel(newLabel: string);
  abstract deleteLabel(deleteLabel: string);
  abstract createLabelledNote(note: Note);
  abstract removeLabelledNote(note: Note);
  // Predefined
  abstract getAllLabels(): Observable<string[]>;
  abstract changeLook(): NoteLook;
  abstract getNotes(type: NoteType): Observable<Note[]>;
  abstract Add(note: Note): void;
  abstract Remove(note: Note): void;
  abstract AddToTrash(note: Note): void;
  abstract RemoveFromTrash(note: Note): void;
  abstract updateNotes(note: Note): void;
}
