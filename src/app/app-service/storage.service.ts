import { Injectable } from '@angular/core';
import { IStorageService } from './IStorage.service';
import { Note } from '../Models/Note';
import { NoteType } from '../Models/NoteType';
import { NoteLook } from '../Models/NoteLook';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StorageService extends IStorageService {
  private notes: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>([]);
  private trashNotes: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>([]);
  private archiveNotes: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>(
    [],
  );
  private reminderNotes: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>(
    [],
  );
  private customLabels: BehaviorSubject<string[]> = new BehaviorSubject<
    string[]
  >([]);
  private labelKeys: Map<string, BehaviorSubject<Note[]>> = new Map<
    string,
    BehaviorSubject<Note[]>
  >();

  noteLook: NoteLook;

  constructor() {
    super();
    this.noteLook = JSON.parse(localStorage.getItem('look')) || 0;
    this.refreshNotes();
    this.createLabelledState();
  }

  // customLabels

  createLabelledState() {
    let labels;
    this.getAllLabels().subscribe((data) => (labels = data));

    labels.forEach((label, index) => {
      const temp = new BehaviorSubject<Note[]>(
        JSON.parse(localStorage.getItem(label)) || [],
      );
      this.labelKeys.set(label, temp);
    });
  }

  getAllLabels(): Observable<string[]> {
    const label = JSON.parse(localStorage.getItem('customLabels')) || [];
    this.customLabels.next(label);
    return this.customLabels;
  }

  getNotesByLabel(label: string): Observable<Note[]> {
    const noteList = JSON.parse(localStorage.getItem(label)) || [];
    const obs = this.labelKeys.get(label);
    if (obs !== undefined) {
      obs.next(noteList);
    }
    return obs;
  }

  createLabel(newLabel: string) {
    let label = JSON.parse(localStorage.getItem('customLabels')) || [];
    label.push(newLabel);
    label = label.sort((a, b) => {
      return b.id - a.id;
    });
    localStorage.setItem('customLabels', JSON.stringify(label));

    this.labelKeys.set(newLabel, new BehaviorSubject<Note[]>([]));

    this.customLabels.next(
      JSON.parse(localStorage.getItem('customLabels')) || [],
    );
    localStorage.setItem(newLabel, JSON.stringify(''));
  }

  createLabelledNote(note: Note) {
    const obs = this.labelKeys.get(note.label);
    let bucket = obs.getValue();
    bucket.push({
      id: bucket.length > 0 ? +bucket[0].id + 1 : 1,
      title: note.title,
      content: note.content,
      isDeleted: note.isDeleted,
      isNew: false,
      noteType: note.noteType,
      label: note.label,
      time: undefined,
    });
    bucket = bucket.sort((a, b) => {
      return b.id - a.id;
    });
    localStorage.setItem(note.label, JSON.stringify(bucket));
    obs.next(JSON.parse(localStorage.getItem(note.label)) || []);
    this.labelKeys.set(note.label, obs);
  }

  updateLabelledNote(updateNote: Note) {
    const obs = this.labelKeys.get(updateNote.label);
    const bucket = obs.getValue();

    if (updateNote.id === undefined) {
      this.Add(updateNote);
      return;
    }

    bucket.forEach((note, index) => {
      // tslint:disable-next-line:triple-equals
      if (note.id == updateNote.id) {
        bucket[index].title = updateNote.title;
        bucket[index].content = updateNote.content;
      }
    });
    localStorage.setItem(updateNote.label, JSON.stringify(bucket));
  }

  deleteLabel(deleteLabel: string) {
    const labels = JSON.parse(localStorage.getItem('customLabels')) || [];
    labels.forEach((label, index) => {
      // tslint:disable-next-line:triple-equals
      if (label === deleteLabel) {
        labels.splice(index, 1);
        localStorage.setItem('customLabels', JSON.stringify(labels));
        return;
      }
    });
    localStorage.removeItem(deleteLabel);
    this.labelKeys.delete(deleteLabel);
    this.customLabels.next(labels);
  }

  removeLabelledNote(deleteNote: Note) {
    const obs = this.labelKeys.get(deleteNote.label);
    const bucket = obs.getValue();
    bucket.forEach((note, index) => {
      // tslint:disable-next-line:triple-equals
      if (note.id == deleteNote.id) {
        bucket.splice(index, 1);
        localStorage.setItem(deleteNote.label, JSON.stringify(bucket));
        return;
      }
    });
    localStorage.setItem(deleteNote.label, JSON.stringify(bucket));
    obs.next(JSON.parse(localStorage.getItem(deleteNote.label)) || []);
    this.labelKeys.set(deleteNote.label, obs);
  }

  // existing Category
  refreshNotes() {
    this.notes.next(JSON.parse(localStorage.getItem(NoteType.notes)) || []);

    this.trashNotes.next(
      JSON.parse(localStorage.getItem(NoteType.trash)) || [],
    );

    this.archiveNotes.next(
      JSON.parse(localStorage.getItem(NoteType.archive)) || [],
    );

    this.reminderNotes.next(
      JSON.parse(localStorage.getItem(NoteType.reminder)) || [],
    );

    this.noteLook = JSON.parse(localStorage.getItem('look'));
  }

  getNotes(type: NoteType): Observable<Note[]> {
    const noteList = JSON.parse(localStorage.getItem(type)) || [];
    this.notes.next(noteList);
    if (type === NoteType.notes) {
      this.notes.next(noteList);
      return this.notes;
    } else if (type === NoteType.archive) {
      this.archiveNotes.next(noteList);
      return this.archiveNotes;
    } else if (type === NoteType.reminder) {
      this.reminderNotes.next(noteList);
      return this.reminderNotes;
    } else if (type === NoteType.trash) {
      this.trashNotes.next(noteList);
      return this.trashNotes;
    }
  }

  updateNotes(updatedNote: Note): void {
    if (updatedNote.label !== undefined) {
      this.updateLabelledNote(updatedNote);
      return;
    }
    const noteBucket = this._getNoteBucket(updatedNote.noteType);
    if (updatedNote.id === undefined) {
      this.Add(updatedNote);
      return;
    }
    noteBucket.forEach((note, index) => {
      // tslint:disable-next-line:triple-equals
      if (note.id == updatedNote.id) {
        noteBucket[index].title = updatedNote.title;
        noteBucket[index].content = updatedNote.content;
      }
    });
    localStorage.setItem(updatedNote.noteType, JSON.stringify(noteBucket));
  }

  Add(note: Note): void {
    if (note.label !== undefined) {
      this.createLabelledNote(note);
      return;
    }
    const noteBucket = this._getNoteBucket(note.noteType);
    this._add(note, noteBucket, note.noteType);
  }

  Remove(note: Note) {
    if (note.label !== undefined) {
      this.removeLabelledNote(note);
      return;
    }
    const noteBucket = this._getNoteBucket(note.noteType);
    this._remove(note, noteBucket, note.noteType);
  }

  AddToTrash(note: Note) {
    const noteBucket = this._getNoteBucket(NoteType.trash);
    this._add(note, noteBucket, NoteType.trash);
  }

  RemoveFromTrash(note: Note) {
    const noteBucket = this._getNoteBucket(NoteType.trash);
    this._remove(note, noteBucket, NoteType.trash);
  }

  private _getNoteBucket(type: NoteType) {
    let noteBucket: Note[];
    if (type === NoteType.notes) {
      noteBucket = this.notes.getValue();
    } else if (type === NoteType.archive) {
      noteBucket = this.archiveNotes.getValue();
    } else if (type === NoteType.reminder) {
      noteBucket = this.reminderNotes.getValue();
    } else if (type === NoteType.trash) {
      noteBucket = this.trashNotes.getValue();
    }
    return noteBucket;
  }

  private _add(note: Note, noteBucket: Note[], noteType: NoteType) {
    noteBucket.push({
      id: noteBucket.length > 0 ? +noteBucket[0].id + 1 : 1,
      title: note.title,
      content: note.content,
      isDeleted: note.isDeleted,
      isNew: false,
      noteType: note.noteType,
      time: undefined,
      label: note.label,
    });
    noteBucket = noteBucket.sort((a, b) => {
      return b.id - a.id;
    });
    localStorage.setItem(noteType, JSON.stringify(noteBucket));
    this.refreshNotes();
  }

  private _remove(deleteNote: Note, noteBucket: Note[], noteType: string) {
    noteBucket.forEach((note, index) => {
      // tslint:disable-next-line:triple-equals
      if (note.id == deleteNote.id) {
        noteBucket.splice(index, 1);
        localStorage.setItem(noteType, JSON.stringify(noteBucket));
        return;
      }
    });
    this.refreshNotes();
  }

  changeLook(): NoteLook {
    let x = this.noteLook;
    x = (x + 1) % 3;
    this.noteLook = x;
    localStorage.setItem('noteLook', JSON.stringify(x));
    // tslint:disable-next-line:triple-equals
    if (x == 0) {
      return NoteLook.shapes;
    }
    // tslint:disable-next-line:triple-equals
    if (x == 1) {
      return NoteLook.rectangle;
    }
    // tslint:disable-next-line:triple-equals
    if (x == 2) {
      return NoteLook.list;
    }
  }
}
