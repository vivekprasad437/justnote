export enum NoteType {
  notes = 'notes',
  reminder = 'reminder',
  archive = 'archive',
  trash = 'trash',
  labelled = 'labelled',
}
