export enum NoteAction {
  save,
  create,
  delete,
  restore,
  archive,
  unarchive,
  clone,
  noAction,
  download,
}
