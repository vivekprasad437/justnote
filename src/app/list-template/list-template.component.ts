import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { Note } from '../Models/Note';
import { IStorageService } from '../app-service/IStorage.service';
import { NoteType } from '../Models/NoteType';
import { NoteLook } from '../Models/NoteLook';
import { MatDialog } from '@angular/material/dialog';
import { CreateDialogComponent } from './note-template/create-dialog/create-dialog';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list-template',
  templateUrl: './list-template.component.html',
  styleUrls: ['./list-template.component.scss'],
})
export class ListTemplateComponent implements OnInit {
  noteList: Note[];
  @Input() notes: Observable<Note[]>;
  @Input() noteLook: NoteLook;
  @Input() noteType: NoteType;
  @Input() label: string;

  editorNote: Note;

  ngOnInit(): void {
    this.notes.subscribe((data) => {
      this.noteList = data;
      this.editorNote = this.noteList[0];
    });
  }
  constructor(
    public dialog: MatDialog,
    private storageService: IStorageService,
  ) {
    this.noteLook = this.storageService.noteLook;
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.noteList, event.previousIndex, event.currentIndex);
    localStorage.setItem(this.noteType, JSON.stringify(this.notes));
  }

  setEditor(note: Note) {
    this.editorNote = note;
  }

  updateNote(updatedNote: Note) {
    this.storageService.updateNotes(updatedNote);
  }

  onCreate() {
    const note = new Note();
    note.isNew = true;
    note.isDeleted = false;
    note.noteType = this.noteType;
    note.label = this.label;
    const dialogRef = this.dialog.open(CreateDialogComponent, {
      disableClose: true,
      data: note,
    });
    dialogRef.afterClosed().subscribe(() => {});
  }

  changeLook() {
    this.noteLook = this.storageService.changeLook();
  }
}
