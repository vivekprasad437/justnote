import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NoteType } from 'src/app/Models/NoteType';
import { Note } from '../../../../Models/Note';
import { IStorageService } from '../../../../app-service/IStorage.service';

@Component({
  selector: 'app-note-action',
  templateUrl: './note-action.component.html',
  styleUrls: ['./note-action.component.scss'],
})
export class NoteActionComponent implements OnInit {
  @Input() editorNote: Note;
  @Output() closeEmitter = new EventEmitter();
  @Output() clearEmitter = new EventEmitter();
  @Input() noteType: NoteType;
  @Input() label: string;

  constructor(private storageService: IStorageService) {}

  ngOnInit(): void {}

  setEditor(note: Note) {
    this.editorNote = note;
  }

  clearEditor() {
    this.editorNote.title = '';
    this.editorNote.content = '';
    this.clearEmitter.emit();
  }

  moveToTrash(note: Note) {
    if (note.isDeleted) {
      this.storageService.RemoveFromTrash(note);
    } else {
      note.isDeleted = true;
      this.storageService.Remove(note);
      this.storageService.AddToTrash(note);
    }
    this.editorNote = null;
    this.closeEmitter.emit();
  }

  moveToNotes(note: Note) {
    this.storageService.Remove(note);
    note.noteType = NoteType.notes;
    this.storageService.Add(note);
    this.editorNote = null;
    this.closeEmitter.emit();
  }

  moveToArchiveNote(note: Note) {
    this.storageService.Remove(note);
    note.noteType = NoteType.archive;
    this.storageService.Add(note);
    this.editorNote = null;
    this.closeEmitter.emit();
  }

  cloneNote(note: Note) {
    this.storageService.Add(note);
    this.closeEmitter.emit();
  }

  updateNote(updatedNote: Note) {
    this.storageService.updateNotes(updatedNote);
    this.closeEmitter.emit();
  }

  recycle(note: Note) {
    note.isDeleted = false;
    note.label = this.editorNote.label;
    this.storageService.RemoveFromTrash(note);
    this.storageService.Add(note);
    this.closeEmitter.emit();
  }

  onDownload(note: Note) {
    const fileContent = note.content || '';
    fileContent.replace(/([^\r])\n/g, '$1\r\n');
    const bb = new Blob([fileContent], { type: 'text/plain;' });
    const a = document.createElement('a');
    a.download = note.title;
    a.href = window.URL.createObjectURL(bb);
    a.click();
  }
}
