import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { identifierModuleUrl, ThrowStmt } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { Note } from '../Models/Note';
import { IStorageService } from '../app-service/IStorage.service';
import { NoteLook } from '../Models/NoteLook';
import { NoteType } from '../Models/NoteType';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-trash',
  templateUrl: './trash.component.html',
  styleUrls: ['./trash.component.scss'],
})
export class TrashComponent implements OnInit {
  @Input() trashNotes: Observable<Note[]>;

  noteLook: NoteLook;
  editorNote: Note;
  noteType: NoteType.trash;

  constructor(private storageService: IStorageService) {
    this.trashNotes = this.storageService.getNotes(NoteType.trash);
    this.noteLook = this.storageService.noteLook;
    this.noteType = NoteType.trash;
  }

  ngOnInit(): void {
    this.editorNote = this.trashNotes[0] || null;
  }
}
