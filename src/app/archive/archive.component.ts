import { Component, OnInit } from '@angular/core';
import { Note } from '../Models/Note';
import { IStorageService } from '../app-service/IStorage.service';
import { NoteLook } from '../Models/NoteLook';
import { NoteType } from '../Models/NoteType';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss'],
})
export class ArchiveComponent implements OnInit {
  archiveNotes: Observable<Note[]>;
  noteLook: NoteLook;
  editorNote: Note;
  noteType: NoteType;

  constructor(private storageService: IStorageService) {
    this.archiveNotes = this.storageService.getNotes(NoteType.archive);
    this.noteLook = this.storageService.noteLook;
    this.noteType = NoteType.archive;
  }

  ngOnInit(): void {
    this.editorNote = this.archiveNotes[0] || null;
  }
}
